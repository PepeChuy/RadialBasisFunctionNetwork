#ifndef ADALINERULE_H
#define ADALINERULE_H

#include <vector>

template <class I>
class AdalineRule
{
    public:
        AdalineRule(std::vector<I> inputs, double output);
        const std::vector<I> &inputs() const;
        double output() const;


        std::vector<double> &inputs() { return in; }
        double &output() { return out; }
    private:
        std::vector<I> in;
        double out;
};

#include "adalinerule.cpp"

#endif // ADALINERULE_H

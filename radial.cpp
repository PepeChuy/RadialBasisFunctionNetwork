#include "radial.h"

Radial::Radial()
{
    m_spread = 1;
}

void Radial::setCenter(std::vector<double> newCenter)
{
    m_center = newCenter;
}

std::vector<double> Radial::getCenter()
{
    return m_center;
}

double Radial::getSpread()
{
    return m_spread;
}

void Radial::setSpread(double spread)
{
    m_spread = spread;
}

#include "knownrule.h"

KnownRule::KnownRule(std::vector<double> inputs):
    in(inputs),
    closest(0) {}

const std::vector<double> &KnownRule::inputs() const { return in; }

size_t KnownRule::getClosest()
{
    return closest;
}

void KnownRule::setClosest(size_t newClosest)
{
    closest = newClosest;
}



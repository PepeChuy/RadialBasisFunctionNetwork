#ifndef SCALER_H
#define SCALER_H

#include "adalinerule.h"

#include <vector>
#include <cmath>

class Scaler
{
    public:

        void fit(const std::vector<AdalineRule<double>> &data)
        {
            auto dataRows = data[0].inputs().size() + 1;
            m_means = std::vector<double>(dataRows, 0.0);
            m_stdDevs = std::vector<double>(dataRows, 0.0);
            for (auto &rule : data)
            {
                for (size_t i = 0; i < rule.inputs().size(); ++i)
                {
                    m_means[i] += rule.inputs()[i];
                    m_stdDevs[i] += (rule.inputs()[i] * rule.inputs()[i]);
                }
                m_means.back() += rule.output();
                m_stdDevs.back() += (rule.output() * rule.output());
            }
            for (size_t i = 0; i < dataRows; ++i)
            {
                m_means[i] /= data.size();
                m_stdDevs[i] = std::sqrt(m_stdDevs[i]);
                if (m_stdDevs[i] == 0.0) m_stdDevs[i] = 1.0;
            }
        }

        void transform(AdalineRule<double> &rule) const
        {
            transform(rule.inputs());
            rule.output() -= m_means.back();
            rule.output() /= m_stdDevs.back();
        }

        void transform(std::vector<double> &data) const
        {
            for (size_t i = 0; i < data.size(); ++i)
            {
                data[i] -= m_means[i];
                data[i] /= m_stdDevs[i];
            }
        }

        double inverseTransform(double val) const
        {
            return val * m_stdDevs.back() + m_means.back();
        }

    private:

        std::vector<double> m_means;
        std::vector<double> m_stdDevs;
};

#endif // SCALER_H

#ifndef RBF_H
#define RBF_H

#include <vector>
#include <algorithm>

#include "knownrule.h"
#include "radial.h"
#include "neuron.h"
#include "adalinerule.h"
#include "scaler.h"

class RBF
{
    public:
        enum State
        {
            New,
            TrainingRadial,
            TrainedRadial,
            TrainingAdaline,
            Stopped
        };

        struct RadialEpochResult
        {
            State state;
        };

        struct AdalineEpochResult
        {
            State state;
            double err;
        };

        RBF(size_t k,
            std::vector<KnownRule> rules,
            double learningRate,
            double errTolerance,
            size_t epochsLimit,
            double (*radialFunc)(double, double, double, double),
            double (*activationFunc)(double),
            double (*activationFuncDeriv)(double));

        RadialEpochResult runRadialEpoch();
        AdalineEpochResult runAdalineEpoch();
        std::vector<double> evaluate(std::vector<double> inputs);

        std::vector<Radial> getRadials();

        size_t epochsRun() const;
        double err() const;
        State state() const;

        void prepareAdaline();

    private:

        Neuron* neuron;
        bool recalcClosest();
        void repositionClusters();
        void setSpreads();

        std::vector<double> getRadialOutputs(std::vector<double> inputs);

        std::vector<Radial> m_radials;
        std::vector<KnownRule> m_rules;
        double m_learningRate;
        double m_errTolerance;
        size_t m_epochsLimit;
        double (*m_radialFunc)(double, double, double, double);
        double (*m_fz)(double);
        double (*m_fzDeriv)(double);
        size_t m_epochsRun;
        double m_err;
        State m_state;
        Scaler scaler;
};

#endif // RBF_H

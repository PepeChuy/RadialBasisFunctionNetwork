#ifndef RADIAL_H
#define RADIAL_H

#include <tuple>
#include <vector>
#include <cmath>

class Radial
{
    public:
        Radial();

        double evaluate(double x) { return std::exp(-pow(x - m_center[0], 2) / (2 * std::pow(m_spread, 2))) + m_center[1] - .5; }
        void setCenter(std::vector<double>);
        std::vector<double> getCenter();
        double getSpread();
        void setSpread(double);

    private:
        std::vector<double> m_center;
        double m_spread;
};

#endif // RADIAL_H

#include "neuron.h"

#include <random>       /* std::random_device, std::mt19937,
                           std::uniform_real_distribution */
#include <algorithm>    // std::generate
#include <cmath>        // std::sqrt

#include <iostream>
using namespace std;

Neuron::Neuron(std::vector<AdalineRule<double>> trainingSet,
                             std::tuple<double, double> initSearchRange,
                             double learningRate,
                             double errTolerance,
                             size_t epochsLimit):
    m_expected(trainingSet),
    m_initSearchRange(initSearchRange),
    m_learningRate(learningRate),
    m_errTolerance(errTolerance),
    m_epochsLimit(epochsLimit),
    m_weights(std::vector<double>(trainingSet[0].inputs().size())),
    m_epochsRun(0),
    m_state(New)
{
    // Initialize state
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(
        std::get<0>(initSearchRange), std::get<1>(initSearchRange)
    );
    m_threshold = randDistrib(gen);
    std::generate(m_weights.begin(), m_weights.end(),
        [&]() -> double { return randDistrib(gen); }
    );
}

void printVec(const std::vector<double> &v)
{
    cout << "{ ";
    for (auto x : v)
    {
        cout << x << " ";
    }
    cout << "}" << endl;
}

typename Neuron::EpochResult Neuron::runTrainingEpoch()
{
    if (state() == Stopped) return EpochResult { Stopped, 0.0 };
    m_err = 0.0;

    for (size_t ruleIdx = 0; ruleIdx < trainingSet().size(); ++ruleIdx)
    {
        const auto &rule = trainingSet()[ruleIdx];
        double wx = threshold() * -1.0;
        for (size_t i = 0; i < weights().size(); ++i)
        {
            wx += weights()[i] * rule.inputs()[i];
        }
        double expected = rule.output();
        double err = expected - wx;
        m_err += (err * err);
        m_threshold += (-1.0 * learningRate() * err);
        for (size_t i = 0; i < rule.inputs().size(); ++i)
        {
            m_weights[i] += rule.inputs()[i] * learningRate() * err;
        }
    }

    ++m_epochsRun;
    m_err = m_err / 2.0;
    if (epochsRun() == epochsLimit() || m_err <= m_errTolerance)
    {
        m_state = Stopped;
    }
    else
    {
        m_state = Training;
    }
    return EpochResult { m_state, m_err };
}

double Neuron::evaluate(const std::vector<double> &inputs) const
{
    double wx = threshold() * -1.0;
    for (size_t i = 0; i < weights().size(); ++i)
    {
        wx += weights()[i] * inputs[i];
    }
    return wx;
}

const std::vector<AdalineRule<double>> &Neuron::trainingSet() const
{
    return m_expected;
}

const std::tuple<double, double> &Neuron::initialSearchRange() const
{
    return m_initSearchRange;
}

double Neuron::learningRate() const { return m_learningRate; }

double Neuron::errTolerance() const { return m_errTolerance; }

size_t Neuron::epochsLimit() const { return m_epochsLimit; }

const std::vector<double> &Neuron::weights() const { return m_weights; }

double Neuron::threshold() const { return m_threshold; }

size_t Neuron::epochsRun() const { return m_epochsRun; }

double Neuron::err() const { return m_err; }

typename Neuron::State Neuron::state() const { return m_state; }

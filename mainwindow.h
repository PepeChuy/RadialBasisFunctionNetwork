#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "rbf.h"
#include "knownrule.h"
#include "qcustomplot.h"

#include <tuple>
#include <vector>

#include <QMainWindow>
#include <QMouseEvent>
#include <QTimer>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    public slots:
        void handlePlotClick(QMouseEvent *ev);
        void handleTrainEpoch();
        void updateState();
        void updateGraph();
        void logErr(size_t epoch, double err);
        void graphResults();
        void graphRadials();

    private slots:
        void on_btnInit_clicked();
        void on_btnTrain_clicked();
        void on_btnPause_clicked();
        void on_btnClear_clicked();
        void onPlotModeSelected();

private:
        Ui::MainWindow *ui;
        QTimer clock;

        std::vector<KnownRule> rules;
        RBF *neuralNetwork;

        const std::tuple<double, double> xAxisRange = {-10.0, 10.0};
        const std::tuple<double, double> yAxisRange = {-10.0, 10.0};
        const double xAxisStep = 0.01;
        QCPGraph* rulesGraph;
        QCPGraph* resultGraph;
        std::vector<QCPGraph*> radialGraphs;
        QCPBars *errorGraph;

        QButtonGroup plotModeGroup;

        void graphRadial(QCPGraph*, Radial);

        static double gaussDistrib(double x, double centerX, double centerY, double opening) { return std::exp(-pow(x - centerX, 2) / (2 * std::pow(opening, 2))) + centerY - .5; }
        static double identity(double y) { return y; }
        static double identityDeriv(double y) { return 1; }
};

#endif // MAINWINDOW_H

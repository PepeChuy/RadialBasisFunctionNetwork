#ifndef KNOWNRULE_H
#define KNOWNRULE_H

#include <vector>

class KnownRule
{
    public:
        KnownRule(std::vector<double> inputs);
        const std::vector<double> &inputs() const;
        size_t getClosest();
        void setClosest(size_t);

    private:
        std::vector<double> in;
        size_t closest;
};

#endif // KNOWNRULE_H

#include "adalinerule.h"

template <class I>
AdalineRule<I>::AdalineRule(std::vector<I> inputs, double output):
    in(inputs), out(output) {}

template <class I>
const std::vector<I> &AdalineRule<I>::inputs() const { return in; }

template <class I>
double AdalineRule<I>::output() const { return out; }

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QButtonGroup>
#include <QString>
#include <QSharedPointer>
#include <limits>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    neuralNetwork(nullptr)
{
    auto xMin = std::get<0>(xAxisRange);
    auto xMax = std::get<1>(xAxisRange);
    auto yMin = std::get<0>(yAxisRange);
    auto yMax = std::get<1>(yAxisRange);

    ui->setupUi(this);

    // Plot Mode
    plotModeGroup.addButton(ui->firstLayerRadio, 1);
    plotModeGroup.addButton(ui->outputLayerRadio, 2);

    // Main Plot
    ui->dataPlot->xAxis->setRange(xMin, xMax);
    ui->dataPlot->yAxis->setRange(yMin, yMax);
    rulesGraph = ui->dataPlot->addGraph();
    rulesGraph->setLineStyle(QCPGraph::lsNone);
    rulesGraph->setScatterStyle(QCPScatterStyle(
        QCPScatterStyle::ssCircle, Qt::black, 6.0)
    );

    // Rules Graph
    resultGraph = ui->dataPlot->addGraph();
    resultGraph->setPen(QPen(Qt::black));

    // Error Graph
    errorGraph = new QCPBars(ui->errorPlot->xAxis, ui->errorPlot->yAxis);
    ui->errorPlot->xAxis->setLabel("Epoch");
    ui->errorPlot->yAxis->setLabel("Error");
    QSharedPointer<QCPAxisTickerFixed> fixedTicker(new QCPAxisTickerFixed);
    fixedTicker->setTickStep(1.0);
    fixedTicker->setScaleStrategy(QCPAxisTickerFixed::ssMultiples);
    ui->errorPlot->xAxis->setTicker(fixedTicker);

    // Events
    connect(ui->dataPlot, &QCustomPlot::mouseRelease, this, &MainWindow::handlePlotClick);
    connect(&clock, &QTimer::timeout, this, &MainWindow::handleTrainEpoch);
    connect(&plotModeGroup, static_cast<void(QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &MainWindow::onPlotModeSelected);
}

MainWindow::~MainWindow()
{
    ui->errorPlot->removePlottable(errorGraph);
    delete ui;
    delete neuralNetwork;
}

void MainWindow::handlePlotClick(QMouseEvent *ev)
{
    if (neuralNetwork != nullptr) return;
    auto pos = ev->pos();
    auto x = ui->dataPlot->xAxis->pixelToCoord(pos.x());
    auto y = ui->dataPlot->yAxis->pixelToCoord(pos.y());
    if (x >= std::get<0>(xAxisRange) && x <= std::get<1>(xAxisRange) &&
        y >= std::get<0>(yAxisRange) && y <= std::get<1>(yAxisRange))
    {
        rules.push_back(KnownRule({x, y}));
        rulesGraph->addData(x, y);
        ui->dataPlot->replot();
        ui->btnInit->setEnabled(true);
    }
}

void MainWindow::handleTrainEpoch()
{
    switch (neuralNetwork->state()) {
        case RBF::New:
        case RBF::TrainingRadial:
            neuralNetwork->runRadialEpoch();
            break;
        case RBF::TrainedRadial:
            clock.stop();
            ui->btnTrain->setEnabled(true);
            neuralNetwork->prepareAdaline();
            break;
        case RBF::TrainingAdaline:
            {
                RBF::AdalineEpochResult res = neuralNetwork->runAdalineEpoch();
                logErr(neuralNetwork->epochsRun(), res.err);
            }
            break;
        case RBF::Stopped:
            clock.stop();
            break;
    }
    updateState();
}

void MainWindow::updateState()
{
    using State = RBF::State;
    auto state2Str = [=](State state)
    {
        switch (state)
        {
            case State::TrainingRadial: return "Training Radials";
            case State::TrainedRadial: return "Radials Ready";
            case State::TrainingAdaline: return "Training Adaline";
            case State::Stopped:  return "Stopped";
            default:              return "New";
        }
    };
    ui->editEpoch->setText(QString::number(neuralNetwork->epochsRun()));
    ui->editStatus->setText(state2Str(neuralNetwork->state()));
    ui->editError->setText(QString::number(neuralNetwork->err()));
    updateGraph();
}

void MainWindow::logErr(size_t epoch, double err)
{
    errorGraph->addData(epoch, err);
    ui->errorPlot->rescaleAxes();
    ui->errorPlot->replot();
}

void MainWindow::updateGraph()
{
    for (auto graph : radialGraphs)
    {
        graph->setData({}, {});
    }
    resultGraph->setData({}, {});
    switch (plotModeGroup.checkedId())
    {
        case 1:
            graphRadials();
            break;
        case 2:
            graphResults();
            break;
    }
}

void MainWindow::graphRadial(QCPGraph* graph, Radial radial)
{
//    auto lastVal = gaussDistrib(centerX, centerX, centerY, opening);
//    graph->addData(centerX, lastVal);
//    auto x = xAxisStep;
//    double diff;
//    do {
//        auto res = gaussDistrib(centerX + x, centerX, centerY, opening);
//        diff = lastVal - res;
//        graph->addData(centerX + x, res);
//        graph->addData(centerX - x, res);
//        //qDebug() << gaussDistrib(centerX + x, centerX, centerY, opening);
//        //qDebug() << diff;
//        lastVal = res;
//        x += xAxisStep;
//    } while (diff > radialGraphStopCriterion);
    for (auto x = std::get<0>(xAxisRange); x < std::get<1>(xAxisRange); x += xAxisStep)
    {
        auto res = gaussDistrib(x, radial.getCenter()[0], radial.getCenter()[1], radial.getSpread());
        graph->addData(x, res);
    }
}

void MainWindow::graphRadials()
{
    std::vector<Radial> radials = neuralNetwork->getRadials();
    for (size_t i = 0; i < radials.size(); ++i)
    {
        graphRadial(radialGraphs[i], radials[i]);
    }
    ui->dataPlot->replot();
}

void MainWindow::graphResults()
{
    if (neuralNetwork->state() != RBF::TrainingAdaline && neuralNetwork->state() != RBF::Stopped) return;
    for (auto x = std::get<0>(xAxisRange); x < std::get<1>(xAxisRange); x += xAxisStep)
    {
        auto res = neuralNetwork->evaluate({x});
        resultGraph->addData(x, res[0]);
    }
    ui->dataPlot->replot();
}

void MainWindow::on_btnInit_clicked()
{
    ui->btnInit->setEnabled(false);
    ui->groupConfig->setEnabled(false);

    neuralNetwork = new RBF (
            static_cast<size_t>(ui->spinK->value()),
            rules,
            ui->spinLearnRate->value(),
            ui->spinTolerance->value(),
            static_cast<size_t>(ui->spinEpochsLimit->value()),
            &MainWindow::gaussDistrib,
            &MainWindow::identity,
            &MainWindow::identityDeriv
        );

    for (size_t i = 0; i < static_cast<size_t>(ui->spinK->value()); ++i)
    {
        radialGraphs.push_back(ui->dataPlot->addGraph());
        radialGraphs.back()->setPen(QColor::fromRgb(QRandomGenerator::global()->generate()));
    }

    updateState();
    ui->btnTrain->setEnabled(true);
}

void MainWindow::on_btnTrain_clicked()
{
    ui->btnTrain->setEnabled(false);
    ui->btnPause->setEnabled(true);
    clock.start(ui->spinClock->value());
}

void MainWindow::on_btnPause_clicked()
{
    clock.stop();
    ui->btnTrain->setEnabled(true);
    ui->btnPause->setEnabled(false);
}

void MainWindow::on_btnClear_clicked()
{
    if (clock.isActive()) clock.stop();
    for (auto graph : radialGraphs)
    {
        graph->data()->clear();
    }
    rulesGraph->setData({}, {});
    resultGraph->setData({}, {});
    radialGraphs.clear();
    errorGraph->setData({}, {});
    ui->dataPlot->replot();
    ui->errorPlot->rescaleAxes();
    ui->errorPlot->yAxis->setRangeLower(0.0);
    ui->errorPlot->replot();

    ui->groupConfig->setEnabled(true);
    ui->editEpoch->clear();
    ui->editStatus->clear();
    ui->editError->clear();
    ui->btnInit->setEnabled(true);
    ui->btnTrain->setEnabled(false);
    ui->btnPause->setEnabled(false);

    rules.clear();
    delete neuralNetwork;
    neuralNetwork = nullptr;
}

void MainWindow::onPlotModeSelected()
{
    if (neuralNetwork != nullptr)
    {
        updateGraph();
    }
}

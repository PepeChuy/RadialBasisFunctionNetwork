#include "rbf.h"
#include <cmath>
#include <QDebug>

RBF::RBF(size_t k,
         std::vector<KnownRule> rules,
         double learningRate,
         double errTolerance,
         size_t epochsLimit,
         double (*radialFunc)(double, double, double, double),
         double (*activationFunc)(double),
         double (*activationFuncDeriv)(double)) :
    m_rules(rules),
    m_learningRate(learningRate),
    m_errTolerance(errTolerance),
    m_epochsLimit(epochsLimit),
    m_radialFunc(radialFunc),
    m_fz(activationFunc),
    m_fzDeriv(activationFuncDeriv),
    m_epochsRun(0),
    m_state(New)
{
    std::vector<size_t> found;
    for (size_t i = 0; i < k; ++i)
    {
        m_radials.push_back(Radial());
        size_t idx = static_cast<size_t>(std::rand()) % m_rules.size();
        while (std::find(found.begin(), found.end(), idx) != found.end()) {
            idx = static_cast<size_t>(std::rand()) % m_rules.size();
        }
        found.push_back(idx);
        m_radials.back().setCenter({m_rules.at(idx).inputs()[0], m_rules.at(idx).inputs()[1]});
    }
}

RBF::RadialEpochResult RBF::runRadialEpoch()
{
    bool finished = recalcClosest();
    if (finished)
    {
        setSpreads();
        m_state = TrainedRadial;
    }
    else
    {
        repositionClusters();
        m_state = TrainingRadial;
    }
    return RadialEpochResult { m_state };
}

bool RBF::recalcClosest()
{
    bool finished = true;

//    Distance based on X, Y

    for (auto &rule : m_rules)
    {
        size_t minIdx = 0;
        auto minDst = std::sqrt(std::pow(m_radials[0].getCenter()[0] - rule.inputs()[0], 2) + std::pow(m_radials[0].getCenter()[1] - rule.inputs()[1], 2));
        for (size_t i = 1; i < m_radials.size(); ++i)
        {
            auto dst = std::sqrt(std::pow(m_radials[i].getCenter()[0] - rule.inputs()[0], 2) + std::pow(m_radials[i].getCenter()[1] - rule.inputs()[1], 2));
            if (dst < minDst)
            {
                minDst = dst;
                minIdx = i;
            }
        }
        if (minIdx != rule.getClosest())
        {
            finished = false;
        }
        rule.setClosest(minIdx);
    }

//    Distance based on X

//    for (auto &rule : m_rules)
//    {
//        size_t minIdx = 0;
//        auto minDst = std::abs(m_radials[0].getCenter()[0] - rule.inputs()[0]);
//        for (size_t i = 1; i < m_radials.size(); ++i)
//        {
//            auto dst = std::abs(m_radials[i].getCenter()[0] - rule.inputs()[0]);
//            if (dst < minDst)
//            {
//                minDst = dst;
//                minIdx = i;
//            }
//        }
//        if (minIdx != rule.getClosest())
//        {
//            finished = false;
//        }
//        rule.setClosest(minIdx);
//    }

    return finished;
}

void RBF::repositionClusters()
{
    for (size_t i = 0; i < m_radials.size(); ++i)
    {
        std::vector<double> acc(m_rules[0].inputs().size());
        auto ammount = 0;
        for (auto rule : m_rules)
        {
            if (rule.getClosest() == i)
            {
                for (size_t j = 0; j < rule.inputs().size(); ++j)
                {
                    acc[j] += rule.inputs()[j];
                }
                ++ammount;
            }
        }
        for (size_t j = 0; j < acc.size(); ++j)
        {
            acc[j] /= ammount;
        }
        m_radials[i].setCenter(acc);
    }
}

void RBF::setSpreads()
{
//    What the doc says
    double maxDst = 0;
    for (size_t i = 0; i < m_radials.size() - 1; ++i)
    {
        for (size_t j = i + 1; j < m_radials.size(); ++j)
        {
//            Distance based on X, Y
            auto dst = std::sqrt(std::pow(m_radials[i].getCenter()[0] - m_radials[j].getCenter()[0], 2) + std::pow(m_radials[i].getCenter()[1] - m_radials[j].getCenter()[1], 2));
//            Distance based on X
//            auto dst = std::abs(m_radials[i].getCenter()[0] - m_radials[j].getCenter()[0]);
            if (dst > maxDst)
            {
                maxDst = dst;
            }
        }
    }
    for (auto &radial : m_radials)
    {
        radial.setSpread(maxDst / std::sqrt(m_radials.size()));
    }

//    What I say
//    for (size_t i = 0; i < m_radials.size(); ++i)
//    {
//        auto maxDst = 0.0;
//        auto qty = 0;
//        for (size_t j = 0; j < m_rules.size(); j++)
//        {
//            if (m_rules[j].getClosest() == i)
//            {
//                auto dst = std::sqrt(std::pow(m_radials[i].getCenter()[0] - m_rules[j].inputs()[0], 2) + std::pow(m_radials[i].getCenter()[1] - m_rules[j].inputs()[1], 2));
//                if (dst > maxDst)
//                {
//                    maxDst = dst;
//                }
//                ++qty;
//            }
//        }
//        m_radials[i].setSpread(maxDst / std::sqrt(qty));
//    }
}

void RBF::prepareAdaline()
{
    std::vector<AdalineRule<double>> adalineRules;
    for (auto rule : m_rules)
    {
        adalineRules.push_back(AdalineRule<double>(getRadialOutputs({ rule.inputs()[0] }), rule.inputs()[1]));
    }
    scaler.fit(adalineRules);
    for (auto &rule : adalineRules)
    {
        scaler.transform(rule);
    }
    neuron = new Neuron(
                adalineRules,
                {-1.0, 1.0},
                m_learningRate,
                m_errTolerance,
                m_epochsLimit);
    m_state = TrainingAdaline;
}

RBF::AdalineEpochResult RBF::runAdalineEpoch()
{
//    std::vector<double> outputs;
//    for (auto rule : m_rules)
//    {
//        outputs.push_back(neuron->evaluate(getRadialOutputs({ rule.inputs()[0] })));
//    }
//    std::vector<double> errors;
//    for (size_t i = 0; i < outputs.size(); ++i)
//    {
//        errors.push_back(m_rules[i].inputs()[1] - outputs[i]);
//    }

//    std::vector<double> deltas(neuron->weights.size());
//    for (size_t i = 0; i < errors.size(); ++i)
//    {
//        auto inputs = getRadialOutputs({ m_rules[i].inputs()[0] });
//        inputs.push_back(-1);
//        for (size_t j = 0; j < deltas.size(); ++j)
//        {
//            deltas[j] += errors[i] * inputs[j] * m_learningRate;
//        }
//    }

//    for (size_t i = 0; i < deltas.size(); ++i)
//    {
//        neuron->weights[i] += deltas[i];
//    }

//    m_err = 0;

//    for (auto err : errors)
//    {
//        m_err += err;
//    }
//    m_err *= m_err / 2;
    auto res = neuron->runTrainingEpoch();
    m_err = res.err;
    m_state = (++m_epochsRun == m_epochsLimit || m_err <= m_errTolerance) ? Stopped : TrainingAdaline;

    return RBF::AdalineEpochResult { m_state, m_err };
}

std::vector<double> RBF::evaluate(std::vector<double> inputs)
{
    auto adalineInputs = getRadialOutputs({ inputs[0] });
    scaler.transform(adalineInputs);
    return { scaler.inverseTransform(neuron->evaluate(adalineInputs)) };
}

std::vector<double> RBF::getRadialOutputs(std::vector<double> inputs)
{
    std::vector<double> outputs;
    for (auto radial : m_radials)
    {
//        auto x = m_radialFunc(inputs[0], radial.getCenter()[0], radial.getCenter()[1], radial.getSpread());
//        outputs.push_back(x > 0.1 ? x : 0);
        outputs.push_back(m_radialFunc(inputs[0], radial.getCenter()[0], radial.getCenter()[1], radial.getSpread()));
    }
    return outputs;
}

std::vector<Radial> RBF::getRadials()
{
    return m_radials;
}

size_t RBF::epochsRun() const
{
    return m_epochsRun;
}

double RBF::err() const
{
    return m_err;
}

RBF::State RBF::state() const
{
    return m_state;
}

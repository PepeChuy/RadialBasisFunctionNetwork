#ifndef NEURON_H
#define NEURON_H

#include "adalinerule.h"

#include <vector>
#include <tuple>

class Neuron
{
    public:
        enum State
        {
            New,
            Training,
            Stopped
        };

        struct EpochResult
        {
            State state;
            double err;
        };

        Neuron(std::vector<AdalineRule<double>> trainingSet,
                std::tuple<double, double> initSearchRange,
                double learningRate,
                double errTolerance,
                size_t epochsLimit);

        EpochResult runTrainingEpoch();
        double evaluate(const std::vector<double> &inputs) const;

        const std::vector<AdalineRule<double>> &trainingSet() const;
        const std::tuple<double, double> &initialSearchRange() const;
        double learningRate() const;
        double errTolerance() const;
        size_t epochsLimit() const;
        const std::vector<double> &weights() const;
        double threshold() const;
        size_t epochsRun() const;
        double err() const;
        State state() const;
    private:
        std::vector<AdalineRule<double>> m_expected;
        std::tuple<double, double> m_initSearchRange;
        double m_learningRate;
        double m_errTolerance;
        size_t m_epochsLimit;
        std::vector<double> m_weights;
        double m_threshold;
        size_t m_epochsRun;
        double m_err;
        State m_state;
};

#endif // NEURON_H
